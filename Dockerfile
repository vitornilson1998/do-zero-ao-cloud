FROM maven:3.8.4-openjdk-11 as builder

WORKDIR /do-zero-ao-cloud

COPY pom.xml /do-zero-ao-cloud/pom.xml

RUN mvn dependency:go-offline -B

COPY ./src /do-zero-ao-cloud/src
COPY application.properties /do-zero-ao-cloud/src/main/resources/application.properties

RUN mvn clean package -DskipTests

FROM arm64v8/openjdk:11.0.14-jdk
WORKDIR /do-zero-ao-cloud

COPY --from=builder /do-zero-ao-cloud/target/*.jar  /do-zero-ao-cloud/aplicacao.jar
EXPOSE 8080

CMD ["java", "-jar", "aplicacao.jar"]