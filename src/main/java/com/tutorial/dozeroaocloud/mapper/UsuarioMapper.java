package com.tutorial.dozeroaocloud.mapper;

import com.tutorial.dozeroaocloud.dto.request.UsuarioRequestDto;
import com.tutorial.dozeroaocloud.model.Usuario;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UsuarioMapper extends BasicMapper<Usuario, UsuarioRequestDto> {

    @Autowired
    public UsuarioMapper(ModelMapper modelMapper) {
        super(modelMapper);
    }

    @Override
    public Class<Usuario> entityClass() {
        return Usuario.class;
    }

    @Override
    public Class<UsuarioRequestDto> dtoClass() {
        return UsuarioRequestDto.class;
    }
}
