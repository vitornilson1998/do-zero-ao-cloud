package com.tutorial.dozeroaocloud.mapper;

import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;

@AllArgsConstructor
public abstract class BasicMapper<T, S> {

    private ModelMapper modelMapper;

    public T toEntity(S dto){
        return modelMapper.map(dto, entityClass());
    }

    public S toDto(T entity){
        return modelMapper.map(entity, dtoClass());
    }

    public abstract Class<T> entityClass();
    public abstract Class<S> dtoClass();


}
