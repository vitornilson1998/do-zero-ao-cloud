package com.tutorial.dozeroaocloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DozeroaocloudApplication {

	public static void main(String[] args) {
		SpringApplication.run(DozeroaocloudApplication.class, args);
	}

}
