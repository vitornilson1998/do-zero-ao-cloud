package com.tutorial.dozeroaocloud.service;

import com.tutorial.dozeroaocloud.mapper.BasicMapper;
import com.tutorial.dozeroaocloud.model.EntidadeBase;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;

@AllArgsConstructor
public abstract class BasicService<T extends EntidadeBase, S> {

    private JpaRepository<T, Long> repository;
    private BasicMapper<T, S> basicMapper;

    @Transactional
    public T create(S entidade){
        return repository.save(basicMapper.toEntity(entidade));
    }

    public T findOne(Long id){
        return repository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Entidade Não encontrada"));
    }

    public Page<T> findAll(int page, int size, Sort sort){
        return repository.findAll(PageRequest.of(page, size, sort));
    }

    @Transactional
    public T update(S entidade, Long id){
        repository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Entidade Não encontrada"));
        var entity = basicMapper.toEntity(entidade);
        entity.setId(id);
        return repository.save(entity);
    }

    @Transactional
    public void delete(Long id){
        var optionalEntity = repository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Entidade Não encontrada"));
        repository.delete(optionalEntity);
    }


}
