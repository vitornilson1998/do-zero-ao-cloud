package com.tutorial.dozeroaocloud.service;

import com.tutorial.dozeroaocloud.dto.request.UsuarioRequestDto;
import com.tutorial.dozeroaocloud.mapper.UsuarioMapper;
import com.tutorial.dozeroaocloud.model.Usuario;
import com.tutorial.dozeroaocloud.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UsuarioService extends BasicService<Usuario, UsuarioRequestDto> {

    @Autowired
    private UsuarioRepository repository;
    @Autowired
    private UsuarioMapper basicMapper;

    public UsuarioService(UsuarioRepository repository, UsuarioMapper basicMapper) {
        super(repository, basicMapper);
        this.repository = repository;
        this.basicMapper = basicMapper;
    }
}
