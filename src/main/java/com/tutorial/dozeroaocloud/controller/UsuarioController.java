package com.tutorial.dozeroaocloud.controller;

import com.tutorial.dozeroaocloud.dto.request.UsuarioRequestDto;
import com.tutorial.dozeroaocloud.model.Usuario;
import com.tutorial.dozeroaocloud.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;

@RestController
@RequestMapping("/usuarios")
public class UsuarioController implements ControllerFacade<UsuarioRequestDto, Usuario> {

    @Autowired
    private UsuarioService usuarioService;

    @Override
    @PostMapping
    public ResponseEntity<Usuario> create(@RequestBody UsuarioRequestDto objeto, HttpServletRequest request) {
        return ResponseEntity.created(URI.create(request.getRequestURL().append("/").append(URI.create(
                usuarioService.create(objeto).getId().toString()
        )).toString())).build();
    }

    @Override
    @GetMapping("/{id}")
    public ResponseEntity<Usuario> findOne(@PathVariable("id") Long id) {
        return ResponseEntity.ok(usuarioService.findOne(id));
    }

    @Override
    @GetMapping
    public Page<Usuario> findAll(@RequestParam(value = "page") int page,
                                 @RequestParam("size") int size,
                                 @RequestParam(value = "direction", defaultValue = "ASC") Sort.Direction direction,
                                 @RequestParam(value = "sortBy", defaultValue = "id") String sortBy) {
        return usuarioService.findAll(page, size, Sort.by(direction, sortBy));
    }

    @Override
    @PutMapping("/{id}")
    public ResponseEntity<Usuario> update(@RequestBody UsuarioRequestDto objeto, @PathVariable("id") Long id) {
        return ResponseEntity.ok(usuarioService.update(objeto, id));
    }

    @Override
    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable("id") Long id) {
        usuarioService.delete(id);
        return ResponseEntity.noContent().build();
    }
}
