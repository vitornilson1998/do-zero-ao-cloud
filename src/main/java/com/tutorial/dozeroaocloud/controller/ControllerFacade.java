package com.tutorial.dozeroaocloud.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;

public interface ControllerFacade<T, S> {
    ResponseEntity<S> create(T objeto, HttpServletRequest request);
    ResponseEntity<S> findOne(Long id);
    Page<S> findAll(int page, int size, Sort.Direction direction, String sortBy);
    ResponseEntity<S> update(T objeto, Long id);
    ResponseEntity delete(Long id);
}
